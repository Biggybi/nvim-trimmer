local M = {}

M.setup = function(user_options)
	require("trimmer.config").setup(user_options)
	local options = require("trimmer.config").options
	if options.create_keymaps then
		require("trimmer.trimmer").createKeymaps()
	end
	if options.create_commands then
		require("trimmer.trimmer").createCommands()
	end
	if options.trim_on_save then
		require("trimmer.trimmer").createAutocmd()
	end
end

return M
