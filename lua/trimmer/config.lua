local M = {}

local defaults = {
	pattern = "\\s",
	leader = "dx",
	create_keymaps = true,
	create_commands = true,
	trim_on_save = false,
}

M.options = defaults

M.setup = function(user_options)
	M.options = vim.tbl_extend("force", defaults, user_options or {})
end

return M
