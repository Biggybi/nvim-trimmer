local M = {}

-- trim the 'trim_char' in lines within the 'range'
-- trim_char -> vim *regexp*
-- side -> where to trim
-- - tail (default) -> end of the line
-- - head -> start of the line
-- - both -> start and end of the line
M.trim_lines = function(range, pattern, side)
	local options = require("trimmer.config").options
	local motion_range = "'[,']"
	range = range or motion_range
	pattern = "\\(" .. (pattern or options.pattern) .. "\\)"

	if side == "tail" or not side then
		pattern = pattern .. "\\+$"
	elseif side == "head" then
		pattern = "^" .. pattern .. "\\+"
	elseif side == "both" then
		pattern = "^" .. pattern .. "\\+\\|" .. pattern .. "\\+$"
	end

	vim.cmd("silent! keeppatterns " .. range .. "s/" .. pattern .. "//g")
end

M.trim_operator = function(...)
	if select("#", ...) == 0 then
		vim.o.operatorfunc = "v:lua.require'trimmer.trimmer'.trim_operator"
		return "g@"
	end
	M.trim_lines()
end

function M.createAutocmd()
	vim.api.nvim_create_autocmd({ "BufWritePre" }, {
		pattern = "*",
		group = vim.api.nvim_create_augroup("nvim-trimmer", {}),
		callback = function()
			M.trim_lines("0,$")
		end,
	})
end

function M.createKeymaps()
	local leader = require("trimmer.config").options.leader
	vim.keymap.set("n", leader, M.trim_operator(), { desc = "Trim operator" })
	vim.keymap.set("n", leader .. leader:sub(-1), M.trim_operator() .. "_", { desc = "Trim line" })
end

function M.createCommands()
	vim.api.nvim_create_user_command("Trim", function(o)
		M.trim_lines(o.line1 .. "," .. o.line2, o.fargs[1], o.bang and "both" or "tail")
	end, { nargs = "?", range = 1, bang = true, desc = "Trim line with pattern" })

	vim.api.nvim_create_user_command("TrimHead", function(o)
		M.trim_lines(o.line1 .. "," .. o.line2, o.fargs[1], o.bang and "both" or "head")
	end, { nargs = "?", range = 1, bang = true, desc = "Trim line with pattern from head" })
end

return M
