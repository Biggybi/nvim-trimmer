# trimmer - a plugin for easy trimming

## features
- command and mappings
- operator function (compatible with motions and text-objects as `ap`)
- visual-mode
- dot-repeatable
- sane defaults
- auto-trim on save (opt-in)

## installation

Clone into your `&runtimepath` or use your favorite plugin manager.

For [lazy.nvim](https://github.com/folke/lazy.nvim)

``` lua
  {
    "https://gitlab.com/Biggybi/nvim-trimmer.git",
    opts = {},
  }
```

## configaration

``` lua
{
    pattern = "\\s", -- pattern to remove from line ("\\s": spaces)
    create_mappings = true, -- whether to create mappings (true)
    create_commands = true, -- whether to create commands (true)
    trim_on_save = false, -- whether to trim on save (false)
}
```

### create your mappings
```lua
-- motion mapping
lua vim.keymap.set("n", "<leader>x", require'trimmer'.trim_operator())
-- current line mapping
lua vim.keymap.set("n", "<leader>xx", require'trimmer'.trim_operator() .. "_")
```

## usage

### mappings

#### normal mode
- `dx{motion}` / `d<space>{motion}`: trim lines on *{motion}* (e.g. `dxap` for paragraph)
- `dxx` / `d<space><space>`: trim current line

#### visual mode
- `<space>dx` / `<space>d<space>`: trim selection

### command

- [range]Trim[!] [pos] [pattern]

  - range: vim *range*
  - pattern: vim *pattern* for trimming
  - pos: [ t[ail] | h[ead] | b[oth] | a[ll] | n[none] | p[attern] | * ]
  	- tail: trim at the end of the line (default)
    - head: trim at the start of the line
    - both: trim at the start and at the end of line
    - all|none|pattern|*: remove the pattern anywhere in the line
  - bang: same as 'both'
